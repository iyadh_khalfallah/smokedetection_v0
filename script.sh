# Repeat the process through an infinite loop
while true
do

        python3 connect.py

        python3 SmokeDetection/yolov5/detect.py --weights SmokeDetection/yolov5/runs/train/yolov5s_results/weights/best.pt --img 416 --conf 0.6 --source images/ --save-txt --nosave

        python3 send.py

        rm images/*

        rm -r runs/detect/exp*

        sleep 45
done

# The steps are the following:
# - Connect the FTP Host and get the last 10 images (L 5).
# - Run the smoke detection algorithm through the images and save the labels (L 7).
# - Send the images with smoke labels through the API to send emails (L 9).
# - Delete the images and experiments so that the code runs on the next iterations with new images (L 11, 13).