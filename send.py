import requests
from os import listdir

# Get the images containing labels.
set_folders = set()
images = []
for f in listdir('runs/detect/exp/labels'):
    path = f.split('__')
    if path[0] in set_folders:
        continue
    else:
        set_folders.add(path[0])
        images.append([path[0], path[1]])

# Send the images with labels via the API to send an email.
for path in images:
    url = "https://wtf.provincialtelecom.ca/api/detect?folder="+str(path[0])
    print(path[1].replace('.txt', '.jpg'))
    print("*****")
    data = {"file": open(
        'images/'+str(path[0])+'__'+str(path[1]).replace('.txt', '.jpg'), 'rb')}
    requests.post(url, files=data)
